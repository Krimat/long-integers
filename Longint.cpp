#include<iostream>
#include"LongInt.h"
#include"LongIntErrors.h"


//constructors
Longint::Longint() {
	InitByZero();
}

Longint::Longint(int value) {
	if (value) {
		int to_move = 0;
		while (value)
		{
			to_move = value % Longint::base;
			digits.push_back(to_move);
			value -= to_move;
			value /= Longint::base;
		}

	}
	else {
		InitByZero();
	}
}

Longint::Longint(std::string& value) {
	int string_len = value.size();
	int len = string_len / 3 + 1;
	digits.resize(len);
	for (size_t i = 0; i < string_len; i++)
	{
		digits[i / 3] += (static_cast<int>(value[string_len - 1 -i])- static_cast<int>('0')) * std::pow(10,i%3);
	}

	normalize();
}


//additive operators
Longint operator+(const  Longint& first, const Longint& second) {
	int first_len = first.digits.size();
	int second_len = second.digits.size();
	return sum(first.digits, second.digits, 0, first_len, 0, second_len);
}

Longint operator-(const  Longint& first, const Longint& second) {
	int first_len = first.digits.size();
	int second_len = second.digits.size();
	if (first_len >= second_len) {
		return sub(first.digits, second.digits);
	}
	else {
		std::string error = "Error in '-' operator";
		throw LessThanZero(error);
	}
}


//additive methods

Longint sub(const std::vector<int> first, const std::vector<int> second) {
	int first_len = first.size();
	int second_len = second.size();
	Longint result = Longint();

	result.digits.resize(first_len);

	bool carry = false;
	int less = 0;
	for (size_t i = 0; i < first_len; i++)
	{
		
		if (i < second_len) {
			less = second[i];
		}
		else {
			less = 0;
		}
		result.digits[i] = first[i] - less- carry;
		if (result.digits[i] < 0) {
			carry = true;
			result.digits[i] += Longint::base;
		}
		else {
			carry = false;
		}
	}
	result.normalize();
	return result;

}



//unsafe fuction
//use only if you know how it works
Longint sum(const std::vector<int>& first, const std::vector<int>& second, int f_start, int f_end,int s_start,int s_end) {

	int first_len = first.size();
	int second_len = second.size();
	

	int first_len_to_write = f_end - f_start;
	int second_len_to_write = s_end - s_start;
	Longint result = Longint();
	bool test = first_len_to_write > second_len_to_write;
	if (first_len_to_write > second_len_to_write) {
		result.digits.resize(first_len_to_write + 1);
	}
	else {
		result.digits.resize(second_len_to_write + 1);
	}
	for (size_t i = 0; i < first_len_to_write ||i<second_len_to_write; i++)
	{
		if (((i + f_start) < f_end) && ((i+f_start)<first_len)) {
			result.digits[i] += first[i + f_start];
			if (result.digits[i] >= Longint::base) {
				result.digits[i + 1] += 1;
				result.digits[i] -= Longint::base;
			}
		}
		if (((i + s_start) < s_end) && ((i + s_start) < second_len)) {
			result.digits[i] += second[i + s_start];
			if (result.digits[i] >= Longint::base) {
				result.digits[i + 1] += 1;
				result.digits[i] -= Longint::base;
			}
		}
	}


	result.normalize();
	return result;
}



//multiplicative methods

Longint square_method(const  Longint& first, const Longint& second) {
	Longint result = Longint();

	int first_len = first.digits.size();
	int second_len = second.digits.size();

	int new_len = first_len + second_len;
	result.digits.resize(new_len+1);

	for (size_t i = 0; i < first_len; i++)
	{
		for (size_t j = 0; j < second_len; j++)
		{
			if (result.digits[i + j] >= Longint::base) {
				result.digits[i + j] -= Longint::base;
				result.digits[i + j + 1] += 1;
			}
			result.digits[i + j] += first.digits[i] * second.digits[j];
			if (result.digits[i + j] >= Longint::base) {
				int old = result.digits[i + j];
				int new_res = old % Longint::base;
				old -= new_res;
				old /= Longint::base;
				result.digits[i + j + 1] += old;
				result.digits[i + j] = new_res;
			}			
			if (result.digits[i + j+1] >= Longint::base) {
				int old = result.digits[i + j+1];
				int new_res = old % Longint::base;
				old -= new_res;
				old /= Longint::base;
				result.digits[i + j + 2] += old;
				result.digits[i + j+1] = new_res;
			}
		}
	}
	result.normalize();
	return result;
}

Longint Karatsuba(const  Longint& first, const Longint& second) {

	int first_len = first.digits.size();
	int second_len = second.digits.size();

	if (first_len > second_len) {
		return Karatsuba_inner(first.digits, second.digits, 0, first_len);
	}
	else {
		return Karatsuba_inner(second.digits, first.digits, 0, second_len);
	}

}


Longint Karatsuba_inner(const  std::vector<int>& first, const std::vector<int>& second, int start_s, int end_s) {
	int len = end_s - start_s;
	
	int sum_len = len + 1;

	if (len == 1) {
		if (start_s >= second.size()) {
			return Longint(0);
		}
		else {
			return Longint(first[start_s] * second[start_s]);
		}
		
	}
	
	Longint highest_digits = Karatsuba_inner(first, second,start_s + len / 2 + len % 2,start_s + len);
	
	Longint lowest_digits = Karatsuba_inner(first, second, start_s, start_s + len / 2 + len % 2);
	Longint high_digits = sum(first, first, start_s, start_s + len / 2 + len%2 , start_s + len / 2 + len % 2, start_s + len) * sum(second, second, start_s, start_s + len / 2 + len%2 , start_s + len / 2 + len % 2, start_s + len ) - lowest_digits - highest_digits;
	/*std::cout << start_s<< "  "<<end_s <<std::endl;
	highest_digits.print();
	high_digits.print();
	lowest_digits.print();*/
	highest_digits.shift((len + len%2));
	high_digits.shift((len) / 2 + len%2);
	return highest_digits + lowest_digits + high_digits;
}

Longint Toom_Cook(const  Longint& first, const Longint& second) {
	int first_len = first.digits.size();
	int second_len = second.digits.size();

	if (first_len > second_len) {
		return Toom_Cook_inner(first.digits, second.digits, 0, first_len);
	}
	else {
		return Toom_Cook_inner(second.digits, first.digits, 0, second_len);
	}
}
Longint Toom_Cook_inner(const  std::vector<int>& first, const std::vector<int>& second, int start_s, int end_s) {
	return Longint(0);
}


//multiplicative operators

Longint operator*(const  Longint& first, const Longint& second) {
	return Karatsuba(first, second);
}


//public methods

void Longint::print() {
	int length = digits.size();
	
	for (size_t i = 0; i < length; i++)
	{
		std::cout << digits[length - i - 1] << " ";
	}
	std::cout << std::endl;
}

int Longint::get_int() {
	int result = 0;
	int length = digits.size();
	if (length == 1) {
		return digits[0];
	}
	for (size_t i = 0; i < length; i++)
	{
		result += digits[i]*static_cast<int>(std::pow(Longint::base, i));
	}
	return result;
}
//private methods
void Longint::normalize() {
	int length = digits.size();
	for (size_t i = 0; i < length && !digits[length - i - 1]; i++)
	{
		digits.pop_back();
	}
	if (digits.size() == 0) {
		digits.push_back(0);
	}
}

void Longint::str_normalize() {
	digits.push_back(0);
	int length = digits.size();
	
	for (size_t i = 0; i < length; i++)
	{
		if (digits[i] >= Longint::base) {
			int old = digits[i];
			int new_res = old % Longint::base;
			old -= new_res;
			old /= Longint::base;
			digits[i + 1] += old;
			digits[i] = new_res;
		}
	}
	normalize();
}

void Longint::shift(int shift_length) {
	int prev_len = digits.size();
	int new_len = prev_len + shift_length;
	digits.resize(new_len);
	for (size_t i = 0; i < prev_len; i++)
	{
		digits[new_len - i - 1] = digits[prev_len - i - 1];
		digits[prev_len - i - 1] = 0;
	}
}

void Longint::InitByZero() {
	digits.resize(1);
	digits[0] = 0;
}
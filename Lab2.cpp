#include <iostream>
#include "LongInt.h"
#include"LongIntErrors.h"
#include <vector>
#include<ctime>
#include<chrono>
#include<exception>

std::vector<int> test(int out_len,int inn_len) {
	std::vector<int> result;
	for (size_t i = 0; i < out_len; i++)
	{
		for (size_t j = 0; j < inn_len; j++)
		{
			int ij = i * j;
			Longint mult = Longint(ij);
			Longint li = Longint(i);
			Longint lj = Longint(j);
			if ((li * lj) == mult && ij == mult.get_int()) {
				if (!(j % 1000)) {
					std::cout << i << "  " << j << std::endl;
				}
			}
			else {
				result.push_back(i);
				result.push_back(j);
				return result;
				std::cout << i << "  " << j << std::endl;

			}
		}
	}
	return result;
}


void test_speed_empty(int out_len, int inn_len) {
	for (size_t i = 0; i < out_len; i++)
	{
		for (size_t j = 0; j < inn_len; j++)
		{
			Longint il = Longint(i);
			Longint jl = Longint(j);
			
		}
	}
}

void test_speed_whole_empty(int len) {
	Longint a = Longint(1);
	for (size_t i = 0; i < len; i++)
	{

	}
}

void test_speed_empty(int len) {
	Longint a = Longint(1);
	for (size_t j = 0; j < len; j++)
	{
		a = Longint(j);

	}


}



void test_speed_karatsuba(int out_len, int inn_len ) {
	for (size_t i = 0; i < out_len; i++)
	{
		for (size_t j = 0; j < inn_len; j++)
		{
			Longint il = Longint(i);
			Longint jl = Longint(j);
			(il * jl);
		}
	}
}


void test_speed_karatsuba(int len) {
	Longint a = Longint(1);
	for (size_t j = 1; j < len; j++)
	{
		a = a*Longint(j);

	}
}
void test_speed_square(int out_len, int inn_len ) {
	for (size_t i = 0; i < out_len; i++)
	{
		for (size_t j = 0; j < inn_len; j++)
		{
			Longint il = Longint(i);
			Longint jl = Longint(j);
			square_method(il, jl);
		}
	}
}
void test_speed_square(int len) {
	Longint a = Longint(1);
	for (size_t j = 0; j < len; j++)
	{
		a = square_method(a, Longint(j));

	}
}

/*void main_test() {
	int out_len = 1000;
	int inn_len = 1000;

	auto start_t_karatsuba = std::chrono::system_clock::now();
	test_speed_karatsuba(out_len, inn_len);
	auto end_t_karatsuba = std::chrono::system_clock::now();
	auto start_t_square = std::chrono::system_clock::now();
	test_speed_karatsuba(out_len, inn_len);
	auto end_t_square = std::chrono::system_clock::now();
	auto start_t_empty = std::chrono::system_clock::now();
	test_speed_karatsuba(out_len, inn_len);
	auto end_t_empty = std::chrono::system_clock::now();

	std::cout << "Empty " << end_t_empty - start_t_empty << std::endl;
	std::cout << "Karatsuba: " << end_t_karatsuba - start_t_karatsuba << std::endl;
	std::cout << "Square: " << end_t_square - start_t_square << std::endl;
}*/
void main_linear() {
	int len = 400;

	auto start_t_wempty = std::chrono::system_clock::now();
	test_speed_whole_empty(len);
	auto end_t_wempty = std::chrono::system_clock::now();
	auto start_t_karatsuba = std::chrono::system_clock::now();
	test_speed_karatsuba(len);
	auto end_t_karatsuba = std::chrono::system_clock::now();
	auto start_t_square = std::chrono::system_clock::now();
	test_speed_karatsuba(len);
	auto end_t_square = std::chrono::system_clock::now();
	auto start_t_empty = std::chrono::system_clock::now();
	test_speed_karatsuba(len);
	auto end_t_empty = std::chrono::system_clock::now();


	std::chrono::duration<double> elapsed_wempty = end_t_wempty - start_t_wempty;
	std::chrono::duration<double> elapsed_empty = end_t_empty - start_t_empty;
	std::chrono::duration<double> elapsed_karatsuba = end_t_karatsuba - start_t_karatsuba;
	std::chrono::duration<double> elapsed_square = end_t_square - start_t_square;
	std::cout << "Whole empty:  " << elapsed_wempty.count() << std::endl;
	std::cout << "Empty:  " << elapsed_empty.count() << std::endl;
	std::cout << "Karatsuba: " << elapsed_karatsuba.count() << std::endl;
	std::cout << "Square: " << elapsed_square.count() << std::endl;
}

void test_2_i_on_correct(int max_pow) {
	for (size_t i = 0; i < max_pow; i++)
	{
		std::cout << "Pow" << i + 1 << std::endl;
		Longint tested = static_cast<int>(pow(2, i + 1));
		(tested * tested).print();
		square_method(tested, tested).print();
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
	}
}
void test_3_i_on_correct(int max_pow) {
	for (size_t i = 0; i < max_pow; i++)
	{
		std::cout << "Pow" << i + 1 << std::endl;
		Longint tested = static_cast<int>(pow(2, i + 1));
		(tested * tested).print();
		square_method(tested, tested).print();
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
	}
}

int main()
{
	/*int ash = 1;
	int bsh = 1000;
	Longint a = Longint(ash);
	Longint b = Longint(bsh);
	a == b;
	(a * b).print();
	square_method(a, b).print();
	std::cout << (a * b).get_int();*/


	/*int out = 10000;
	int inn = 10000;
	std::vector<int> wrong = test(out, inn);
	if (wrong.size() == 0) {
		std::cout << "Everything ok" << std::endl;
	}
	else {
		std::cout << wrong[0] << "  " << wrong[1] << std::endl;
	}*/



	/*std::string tested= "1606938044258990275541962092341162602522202993782792835301376";
	Longint string_test = Longint(tested);
	string_test.print();
	auto start_t_karatsuba = std::chrono::system_clock::now();
	(string_test* string_test).print();
	auto end_t_karatsuba = std::chrono::system_clock::now();
	auto start_t_square = std::chrono::system_clock::now();
	square_method(string_test, string_test).print();
	auto end_t_square = std::chrono::system_clock::now();


	std::chrono::duration<double> elapsed_karatsuba = end_t_karatsuba - start_t_karatsuba;
	std::chrono::duration<double> elapsed_square = end_t_square - start_t_square;
	//test_2_i_on_correct(40);
	std::cout << "Karatsuba: " << elapsed_karatsuba.count() << std::endl;
	std::cout << "Square: " << elapsed_square.count() << std::endl;*/

	//main_test();
	main_linear();
}



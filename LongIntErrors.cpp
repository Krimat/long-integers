#include"LongIntErrors.h"
LessThanZero::LessThanZero(std::string& message) :m_message{ message } {

}
const char* LessThanZero::what() const noexcept {
	return m_message.c_str();
}

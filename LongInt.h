#pragma once
#include<vector>
class Longint {
public:

	static const int base = 1000;

private:

	std::vector<int> digits;

public:

	//constructors
	Longint();
	Longint(int value);
	Longint(std::string& value);

	//additive operators
	friend Longint operator+(const  Longint& first, const Longint& second);
	friend Longint operator-(const  Longint& first, const Longint& second);

	//additive methods
	friend Longint sum(const std::vector<int>& first, const std::vector<int>& second, int f_start, int f_end, int s_start, int s_end);
	friend Longint sub(const std::vector<int> first, const std::vector<int> second);

	//multiplicative methods
	friend Longint square_method(const  Longint& first, const Longint& second);


	//comparsion operators operators
	friend bool operator==(const Longint& first, const Longint& second) {
		int first_len = first.digits.size();
		int second_len = second.digits.size();
		if (first_len != second_len) {
			return false;
		}
		else {
			int len = first_len;
			for (size_t i = 0; i < first_len; i++)
			{
				
				if (first.digits[len - 1 -i] == second.digits[len - 1 - i]) {

				}
				else {
					return false;
				}
			}
		}
		return true;
	}

	//Karatsuba algorithms
	friend Longint Karatsuba(const  Longint& first, const Longint& second);
	friend Longint Karatsuba_inner(const  std::vector<int>& first, const std::vector<int>& second, int start_s, int end_s);

	//Toom-Cook algorithm
	friend Longint Toom_Cook(const  Longint& first, const Longint& second);
	friend Longint Toom_Cook_inner(const  std::vector<int>& first, const std::vector<int>& second, int start_s, int end_s);


	//multiplicative operators
	friend Longint operator*(const  Longint& first, const Longint& second);

public:
	//public methods
	void print();
	int get_int();


private:
	//private methods
	void normalize();
	void str_normalize();
	void shift(int shift_length);
	void InitByZero();

};



Longint sum(const std::vector<int>& first, const std::vector<int>& second, int f_start, int f_end, int s_start, int s_end);
Longint sub(const std::vector<int> first, const std::vector<int> second);


Longint Karatsuba(const  Longint& first, const Longint& second);
Longint Karatsuba_inner(const  std::vector<int>& first, const std::vector<int>& second, int start_s, int end_s);

Longint Toom_Cook(const  Longint& first, const Longint& second);
Longint Toom_Cook_inner(const  std::vector<int>& first, const std::vector<int>& second, int start_s, int end_s);
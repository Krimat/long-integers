#pragma once
#include<exception>
#include <string>
#include <iostream>
class BaseError : public std::exception {
public:
	virtual const char* what() const noexcept = 0;
};

class LessThanZero :public BaseError {
private:
	std::string& m_message;
public:
	LessThanZero(std::string& message);
	const char* what() const noexcept;
};
